﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using WebCurso.Models;

namespace WebCurso.Data
{
    public partial class DbCursoContext : DbContext
    {
        public DbCursoContext()
        {
        }

        public DbCursoContext(DbContextOptions<DbCursoContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Calificacion> Calificaciones { get; set; } = null!;
        public virtual DbSet<Comentario> Comentarios { get; set; } = null!;
        public virtual DbSet<Curso> Cursos { get; set; } = null!;
        public virtual DbSet<Instructor> Instructors { get; set; } = null!;
        public virtual DbSet<Precio> Precios { get; set; } = null!;

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Calificacion>(entity =>
            {
                entity.ToView("Calificacion");
            });

            modelBuilder.Entity<Comentario>(entity =>
            {
                entity.HasOne(d => d.Curso)
                    .WithMany(p => p.Comentarios)
                    .HasForeignKey(d => d.CursoId)
                    .HasConstraintName("FK_Comentario_Curso");
            });

            modelBuilder.Entity<Curso>(entity =>
            {
                entity.HasMany(d => d.Instructors)
                    .WithMany(p => p.Cursos)
                    .UsingEntity<Dictionary<string, object>>(
                        "CursoInstructor",
                        l => l.HasOne<Instructor>().WithMany().HasForeignKey("InstructorId").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_CursoInstructor_Instructor"),
                        r => r.HasOne<Curso>().WithMany().HasForeignKey("CursoId").OnDelete(DeleteBehavior.ClientSetNull).HasConstraintName("FK_CursoInstructor_Curso"),
                        j =>
                        {
                            j.HasKey("CursoId", "InstructorId");

                            j.ToTable("CursoInstructor");
                        });
            });

            modelBuilder.Entity<Precio>(entity =>
            {
                entity.HasOne(d => d.Curso)
                    .WithOne(p => p.Precio)
                    .HasForeignKey<Precio>(d => d.CursoId)
                    .HasConstraintName("FK_Precio_Curso");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
