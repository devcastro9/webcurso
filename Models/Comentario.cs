﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace WebCurso.Models
{
    [Table("Comentario")]
    public partial class Comentario
    {
        [Key]
        public int ComentarioId { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? Alumno { get; set; }
        public int? Puntaje { get; set; }
        [Unicode(false)]
        public string? ComentarioTexto { get; set; }
        public int? CursoId { get; set; }

        [ForeignKey("CursoId")]
        [InverseProperty("Comentarios")]
        public virtual Curso? Curso { get; set; }
    }
}
