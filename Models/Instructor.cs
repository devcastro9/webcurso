﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace WebCurso.Models
{
    [Table("Instructor")]
    public partial class Instructor
    {
        public Instructor()
        {
            Cursos = new HashSet<Curso>();
        }

        [Key]
        public int InstructorId { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? Nombres { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? Apellidos { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? Grado { get; set; }

        [ForeignKey("InstructorId")]
        [InverseProperty("Instructors")]
        public virtual ICollection<Curso> Cursos { get; set; }
    }
}
