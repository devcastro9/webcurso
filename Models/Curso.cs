﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace WebCurso.Models
{
    [Table("Curso")]
    public partial class Curso
    {
        public Curso()
        {
            Comentarios = new HashSet<Comentario>();
            Instructors = new HashSet<Instructor>();
        }

        [Key]
        public int CursoId { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? Titulo { get; set; }
        [StringLength(50)]
        [Unicode(false)]
        public string? Descripcion { get; set; }
        [Column(TypeName = "date")]
        public DateTime? FechaPublicacion { get; set; }

        [InverseProperty("Curso")]
        public virtual Precio? Precio { get; set; }
        [InverseProperty("Curso")]
        public virtual ICollection<Comentario> Comentarios { get; set; }

        [ForeignKey("CursoId")]
        [InverseProperty("Cursos")]
        public virtual ICollection<Instructor> Instructors { get; set; }
    }
}
