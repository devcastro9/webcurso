﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace WebCurso.Models
{
    [Table("Precio")]
    [Index("CursoId", Name = "IX_Precio", IsUnique = true)]
    public partial class Precio
    {
        [Key]
        public int Precioid { get; set; }
        [Column(TypeName = "money")]
        public decimal? PrecioActual { get; set; }
        [Column(TypeName = "money")]
        public decimal? PrecioPromocion { get; set; }
        public int? CursoId { get; set; }

        [ForeignKey("CursoId")]
        [InverseProperty("Precio")]
        public virtual Curso? Curso { get; set; }
    }
}
