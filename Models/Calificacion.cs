﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace WebCurso.Models
{
    [Keyless]
    public partial class Calificacion
    {
        [StringLength(50)]
        [Unicode(false)]
        public string? Titulo { get; set; }
        public int? Promedio { get; set; }
        public int? NroCalif { get; set; }
    }
}
