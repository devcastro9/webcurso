﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebCurso.Data;
using WebCurso.Models;

namespace WebCurso.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CalificacionesController : ControllerBase
    {
        private readonly DbCursoContext _context;

        public CalificacionesController(DbCursoContext context)
        {
            _context = context;
        }

        // GET: api/Cursos
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Calificacion>>> GetCursos()
        {
            return await _context.Calificaciones.AsNoTracking().ToListAsync();
        }
    }
}
